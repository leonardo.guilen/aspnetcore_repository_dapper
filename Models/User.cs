using System;
using Dapper.Contrib.Extensions;

namespace repository_dapper.Models
{
    [Table("users")]
    public class User
    {
        [ExplicitKey]
        public string Id { get; set; }
        public string FullName { get; set; }    
        public string Email { get; set; }
        public string Pass { get; set; }
        public DateTime CreatedAt { get; set; }

        public User()
        {
            Id = Guid.NewGuid().ToString();
            CreatedAt = DateTime.Now;
        }
    }
}