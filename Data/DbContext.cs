using System.Data;
namespace repository_dapper.Data
{
    public class DbContext
    {
        private IDbStrategy _dbStrategy;

        public DbContext SetStrategy(string providerType) 
        {
            _dbStrategy = providerType switch {
                "SqlServer" => _dbStrategy = new SqlServerStrategy(),
                "MySql" => _dbStrategy = new MySqlStrategy(),
                _ => null
            };

            return this;
        }

        public IDbConnection GetDbContext(string connectionString) 
        {
            return _dbStrategy.GetConnection(connectionString);
        }
    }
}