using System.Data;

namespace repository_dapper.Data
{
    public interface IDbStrategy
    {
        IDbConnection GetConnection(string connectionString); 
    }
}