using System.Data;
using MySql.Data.MySqlClient;

namespace repository_dapper.Data
{
    public class MySqlStrategy : IDbStrategy
    {
        public IDbConnection GetConnection(string connectionString)
        {
            return new MySqlConnection(connectionString);
        }
    }
}