namespace repository_dapper.Options
{
    public class DatabaseSettings
    {
        public string ProviderName { get; set; }
        public string ConnectionString { get; set; }
    }
}