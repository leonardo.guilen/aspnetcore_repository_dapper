using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using repository_dapper.Models;
using repository_dapper.Repositories;

namespace repository_dapper.Controllers
{
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet("api/users")]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userRepository
                .GetUsersAsync();

            return Ok(users);
        }

        [HttpGet("api/users/{id}")]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var user = await _userRepository
                .GetUserAsync(id);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpPost("api/users")]
        public async Task<IActionResult> Create([FromBody] User request)
        {
            var newUser = new User
            {
                FullName = request.FullName,
                Email = request.Email,
                Pass = request.Pass
            };

            var createdUser = await _userRepository
                .InsertUserAsync(newUser);

            if (!createdUser)
                return BadRequest();

            var absolutedUri = string.Concat(HttpContext.Request.Scheme,
                "://", HttpContext.Request.Host.ToUriComponent(),
                $"/api/users/{newUser.Id}");

            return Created(absolutedUri, newUser);
        }

        [HttpPut("api/users/{id}")]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] User request)
        {
            var user = await _userRepository
                .GetUserAsync(id);

            if (user == null)
                return NotFound();

            user.FullName = request.FullName ?? user.FullName;
            user.Email = request.Email ?? user.Email;

            var updatedUser = await _userRepository
                .UpdateUserAsync(user);

            if (!updatedUser)
                return BadRequest();

            return Ok(user);
        }

        [HttpDelete("api/users/{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var user = await _userRepository
                .GetUserAsync(id);

            if (user == null)
                return NotFound();

            var deletedUser = await _userRepository
                .DeleteUserAsync(id);

            if (!deletedUser)
                return BadRequest();

            return NoContent();
        }
    }
}