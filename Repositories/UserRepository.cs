using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using repository_dapper.Models;
using repository_dapper.Options;

namespace repository_dapper.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DatabaseSettings dbSettings) 
            : base(dbSettings) {}

        public async Task<bool> DeleteUserAsync(Guid id)
        {
            return await DeleteAsync(id);
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            return await FindByIdAsync(id);
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await FindAllAsync();
        }

        public async Task<bool> InsertUserAsync(User user)
        {
            return await CreateAsync(user);
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            return await UpdateAsync(user);
        }
    }
}