using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using repository_dapper.Models;

namespace repository_dapper.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
         Task<IEnumerable<User>> GetUsersAsync();
         Task<User> GetUserAsync(Guid id);
         Task<bool> InsertUserAsync(User user);
         Task<bool> UpdateUserAsync(User user);
         Task<bool> DeleteUserAsync(Guid id);
    }
}